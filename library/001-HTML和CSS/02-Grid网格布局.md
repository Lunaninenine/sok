> Grid 布局与 [Flex 布局](http://www.ruanyifeng.com/blog/2015/07/flex-grammar.html)有一定的相似性，都可以指定容器内部多个项目的位置。但是，它们也存在重大区别。

**Flex 布局是一维布局，Grid可以看作是二维布局，Grid布局远比Flex布局强大。**
## 容器和项目
采用网格布局的区域，称为"容器"（container）。容器内部采用网格定位的子元素，称为"项目"（item）。

## 单元格
行和列的交叉区域，称为"单元格"（cell）

正常情况下，`n`行和`m`列会产生`n x m`个单元格。比如，3行3列会产生9个单元格。

## 网格线
划分网格的线，称为"网格线"（grid line），正常情况下，`n`行有`n + 1`根水平网格线，`m`列有`m + 1`根垂直网格线，比如三行就有四根水平网格线。

## 容器属性
### 1.display 属性

```
display: grid；//整个容器是块级元素
display: inline-grid;//整个容器是行内元素
```

### 2.grid-template-columns 属性， grid-template-rows 属性
```
.container {
  display: grid;
  grid-template-columns: 100px 100px 100px;//定义每一列的列宽
  grid-template-columns: repeat(3, 33.33%);//重复写同样的值时可以使用repeat函数
  grid-template-columns: repeat(auto-fill, 100px);//还可以使用auto-fill 关键字自动填充
  grid-template-columns: 1fr 1fr minmax(100px, 1fr);//minmax(100px, 1fr)表示列宽不小于100px，不大于1fr。
  grid-template-columns: 100px auto 100px;//auto关键字表示由浏览器自己决定长度。
  grid-template-columns: 70% 30%;//百分比布局
  grid-template-columns: repeat(12, 1fr);//实现传统的十二网格布局  
  grid-template-columns: 1fr 1fr;//还可以使用fr关键字表示比例关系
  grid-template-rows: 100px 100px 100px;//定义每一列的列高 
  grid-template-rows: repeat(3, 33.33%);
}
```

### 3.grid-row-gap 属性， grid-column-gap 属性，grid-gap 属性
```
.container {
  grid-row-gap: 20px;
  grid-column-gap: 20px;
}
//上面等同于下面
.container {
  grid-gap: 20px 20px;
}
```

### 4.grid-auto-flow 决定先行后列还是先列后行]

```
grid-auto-flow: column;//先列后行
```
### 5.设置单元格属性：justify-items | align-items  place-items 属性
> justify-items`属性设置单元格内容的水平位置（左中右），`align-items`属性设置单元格内容的垂直位置（上中下），`place-items`属性是`align-items`属性和`justify-items`属性的合并简写形式。

```
.container {
  justify-items: start | end | center | stretch;
  align-items: start | end | center | stretch;
  place-items: <align-items> <justify-items>;
}
```

### 6.justify-content 属性， align-content 属性， place-content 属性
> `justify-content`属性是整个内容区域在容器里面的水平位置（左中右），`align-content`属性是整个内容区域的垂直位置（上中下），`place-content`属性是`align-content`属性和`justify-content`属性的合并简写形式。

```
.container {
  justify-content: start | end | center | stretch | space-around | space-between | space-evenly;
  align-content: start | end | center | stretch | space-around | space-between | space-evenly;   
  place-content:<align-content> <justify-content>
}
```