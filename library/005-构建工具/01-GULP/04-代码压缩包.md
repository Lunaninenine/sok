## 安装包依赖并在gulpfile.js文件中引用

`npm i -D gulp-gzip`

`npm i -D gulp-tar`

`var gzip = require('gulp-gzip')`

`var tar = require('gulp-tar')`

## 新建gulp任务

```js
gulp.task('tar', function() {
    return gulp.src([
        'dist/**/*',//要打包的文件目录
    ]).pipe(tar('person_tax_h5.tar', {'prefix': '/', 'mode': 0755}))//person_tax_h5为压缩包名字
        .pipe(gzip())
        .pipe(gulp.dest('person_tax_h5/'));//输出目录
});
```

## 命令行启服务

`gulp tar`