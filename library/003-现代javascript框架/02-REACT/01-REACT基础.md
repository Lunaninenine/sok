## 1.虚拟DOM

​    在Web开发中，UI界面总是需要根据数据生成对应的DOM由浏览器呈现出来，并随数据的变化而调整相应的DOM,这就需要反复对DOM进行操作。复杂或频繁的DOM操作通常会对性能造成很大的影响。

​    为此React引入了虚拟DOM机制：用哪个户构建虚拟DOM,由React将虚拟DOM渲染到浏览器DOM中。每次数据变化React都会扫描整个虚拟DOM树，自动计算与上次虚拟DOM的差异变化，然后针对需要变化的部分进行实际的浏览器DOM更新。虚拟DOM是内存数据，本身操作性能极高，对实际DOM进行操作的仅仅是差异变化，从而性能得到了很大的提高。

## 2.React生命周期

### 实例化阶段：

getInitialState

componentWillMount

render

componentDidMount

### 活动阶段：

componentWillReceiveProps

shouldComponentUpdate

componentWillUpdate

componentDidUpdate

### 销毁阶段：

componentWillUnmount

## 3.React 哪些数据应该放在state中

state应该包含可能被事件的处理器改变并触发用户界面更新的数据。

## 4.React 哪些数据不应该放在state中

计算所得数据

React组件

基于props的重复数据

## 5.jsx注意事项

for 要写成htmlFor

class 要写成className

## 6.React单向数据流

​	与AngularJS所提倡的双向数据绑定不同。React设计者认为数据双向绑定虽然便捷但在复杂场景下副作用也很明显，相比之下React单向的数据流动---从父节点传递到子节点，基于组件的设计使得组件逻辑简单而且更容易把握，组件只需要从父节点获取prop渲染既可。如果顶层组件的某个prop改变了，React会递归地向下遍历整棵组件树并重新渲染所有使用这个属性的组件。

​	基于单向数据流设计，React提出了单向数据流的架构模式Flux。与MVC分层架构有所区别，Flux以M层Store为核心，围绕Store设计Action和Dispatcher,提供了管理数据的更高级别的框架。	

​	当然，使用ReactLink，React也可以扩展为双向绑定，但不建议使用。

## 7.props成员

​	props是组件固有属性的集合，其数据由外部传入，一般在整个组件的生命周期中都是只读的，React的API设计也决定了这一点。事实上，组件从外界接收静态信息的主要渠道就是props属性。

​	此外，props中也会包含一些由React自动填充的数据，比如this.props.children数组中会包含本组件实例的所有子组件，由React自动填充。如果需要，还可以通过配置实现this.props.context跨级包含上级组件的数据等。

## 8.props与state的区别

props不能被其所在的组件修改，从父组件传进来的属性不会在组件内部更改；state只能在所在组件内部修改，或在外部调用setState函数对状态进行间接修改。

## 9.组件

组件化的目的自然是为了复用

组件外在体现的是可复用性，内在体现则是封装性。

## 10.使用Context来传递数据

可以实现组件树上的数据越级传递。

**使用context时的下级组件必须指定context的数据类型：**

```
contextTypes:{

​	color:React.PropTypes.string

}
```

**使用context时上级组件必须要声明的属性：**

```
childContextTypes:{

​	color:React.PropTypes.string

}
```

**使用context时上级组件必须要声明的回调函数**

```
getChildContext:function(){

​	return {color:"purple"}

}
```

*有声明的回调函数了就不需要在上级组件里面通过绑定自定义属性向下传递了。*

## 11.组件通信

父组件向子组件传递数据是通过props 或者 context

**事件回调机制**

如果子级组件要向父级组件传递信息，该怎么办呢？

通常的方法是父级组件在props中增加一个回调函数，子组件在恰当的时机调用这个回调函数并通知父组件。

对于没有隶属关系的组件间的通信，也可以通过相似的事件机制来实现通信。在componentDidMount()里订阅事件，在componentWillUnmount()里退订，然后在事件回调里调用setState()。前提是组件设计时就约定了相应的事件。

