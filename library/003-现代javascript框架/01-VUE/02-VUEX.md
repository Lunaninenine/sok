## 1.State状态
改变store里state状态值的唯一途径是通过显示的提交mutation.

#### mapState辅助函数
当一个组件需要获取多个状态时，将这些状态都声明为计算属性会有些重复和冗余。为了解决这个问题，我们可以使用mapState辅助函数帮助我们生成计算属性。

使用方法：

store.js

```
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);//使用Vuex

const store = new Vuex.Store({
    state: {
        count: 1,
    },
    mutations:{
        increment (state) {
            state.count++
        },
    }
})
```

组件里:
```
data (){
  return{
    localCount:9
  }
},
computed:{
  //如果一个组件需要用到的状态值比较多的时候可以使用mapState辅助函数
  // count () {
  //   return this.$store.state.count
  // },
  // city () {
  //   return this.$store.city
  // }
  ...mapState({
    //箭头函数可以使代码更简洁
    count:state =>state.count,
    //下面这个countAlias函数，传了字符串参数'count'等同于`state=>state.count`
    countAlias:'count',

    //但有时为了能够使用`this`获取局部状态（当前组件的状态），必须使用常规函数
    countPlusLocalState (state) {
      return state.count + this.localCount
    }
  })
},
```

## 2.getter
有时候我们需要从store中的state中**派生出一些状态**，例如对列表进行过滤并计数。

getter可以认为是store的计算属性！

stores.js:

```
const store = new Vuex.Store({
    state: {
        todos: [
            { id: 1, text: '...', done: true },
            { id: 2, text: '...', done: false },
            { id: 3, text: '...', done: false }
        ]
    },
    getters:{
        doneTodosCount:(state)=>{
            return state.todos.length
        }
    },
    mutations:{
        todosPushItem (state){
            state.todos.push({id: 4, text: '...', done: false })
        }
    }
})
```

组件里

```
<template>
  <div class="home">
    <p>{{doneTodosCount}}</p>
    <p><button @click="todosPushItem">提交mutations里的todosPushItem</button></p>
  </div>
</template>

<script>
import { mapState } from 'vuex';

export default {
  name: 'home',
  computed:{
    ...mapState({
      // doneTodosCount (state,getters) {
      //   return getters.doneTodosCount
      // }
    }),
    
     // 使用对象展开运算符将 getter 混入 computed 对象中
    //使用mapGetters更简洁方便，下面这段mapGetters函数相当于上面注释的代码
    ...mapGetters([
      'doneTodosCount'
    ])
    
  },
  methods:{
    todosPushItem () {
      this.$store.commit('todosPushItem');
    }
  }
}
</script>
```

## 3.mutation
Vuex中的mutation非常类似于事件：每个mutation都有一个字符串的事件类型（**提交mutation时提交的就是一个字符串this.$store.commit('increment')**）和一个回调函数（**increment在mutation里是一个函数**）。

##### 提交载荷（Playload）
可以向store.commit传入额外的参数，即mutation的载荷（payload）:

store.js:

```
mutations:{
    // increment (state,n) {
    //     state.count += n
    //     // state.count++
    // },
    increment (state,payload) {
        state.count += payload.amount
        // state.count++
    
```

组件里：
```
<template>
  <div class="home">
    <p>{{count}}</p>
    <p><button @click="increment">提交increment</button></p>
  </div>
</template>
 methods:{
    increment () {
      // this.$store.commit('increment',10);
      //在大多数情况下，载荷应该是一个对象，这样可以包含多个字段并且记录的mutation会更易读：
      this.$store.commit('increment',{
        amount:10
      });
      //对象风格的提交方式
      this.$store.commit({
        type: 'increment',
        amount:10
      });
    }
  }
```
 **Mutation 必须是同步函数**
 mapMutation
 ```
 import { mapMutations } from 'vuex'

export default {
  // ...
  methods: {
    ...mapMutations([
      'increment', // 将 `this.increment()` 映射为 `this.$store.commit('increment')`
    ]),
    ...mapMutations({
      add: 'increment' // 将 `this.add()` 映射为 `this.$store.commit('increment')`
    })
  }
}
 ```

 ## 4.action