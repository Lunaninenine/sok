## 一、指令

-   v-bind  绑定参数（属性） 例如 v-bind:id=""  ; v-bind:id="'list-' + id" ；也可绑定动态参数：v-bind:[attributeName]="url"
-  绑定class

```
<div v-bind:class="{ active: isActive }"></div>
//绑定多个类样式
<div
  class="static"
  v-bind:class="{ active: isActive, 'text-danger': hasError }"
></div>
//绑定数组
<div v-bind:class="[activeClass, errorClass]"></div>
```
绑定style与绑定class类似。

- v-clock
- v-if
- v-for 
- v-on:click="事件名"；也可绑定动态事件名：v-on:[eventName]="doSomething"
- v-model:轻松实现表单输入和应用状态之间的双向绑定
- v-html
>你的站点上动态渲染的任意 HTML 可能会非常危险，因为它很容易导致 [XSS 攻击](https://en.wikipedia.org/wiki/Cross-site_scripting)。请只对可信内容使用 HTML 插值，**绝不要**对用户提供的内容使用插值。
- v-once 一次性插值（插值处内容不会更新）
- v-show 
*** 带有 `v-show` 的元素始终会被渲染并保留在 DOM 中。`v-show` 只是简单地切换元素的 CSS 属性 `display` ***
>一般来说，`v-if` 是惰性的，有更高的切换开销，而 `v-show` 有更高的初始渲染开销。因此，如果需要非常频繁地切换，则使用 `v-show` 较好；如果在运行时条件很少改变，则使用 `v-if` 较好。
> 当 `v-if` 与 `v-for` 一起使用时，`v-for` 具有比 `v-if` 更高的优先级。
- v-model 在表单元素上创建双向数据绑定 

### 表单修饰符
###### .lazy
###### .number
###### .trim

### 事件修饰符
  ###### .prevent
###### .stop
###### .capture
###### .self
###### .once
###### .native 可能很多次想要在一个组件的根元素上直接监听一个原生事件，用native修饰符
###### .passive  （事件立即触发）vue2.3.0新增的修饰符，尤其能够提升移动端的性能。
  
```
<!-- 阻止单击事件继续传播 -->
<a v-on:click.stop="doThis"></a>

<!-- 提交事件不再重载页面 -->
<form v-on:submit.prevent="onSubmit"></form>

<!-- 修饰符可以串联 -->
<a v-on:click.stop.prevent="doThat"></a>

<!-- 只有修饰符 -->
<form v-on:submit.prevent></form>

<!-- 添加事件监听器时使用事件捕获模式 -->
<!-- 即内部元素触发的事件先在此处理，然后才交由内部元素进行处理 -->
<div v-on:click.capture="doThis">...</div>

<!-- 只当在 event.target 是当前元素自身时触发处理函数 -->
<!-- 即事件不是从内部元素触发的 -->
<div v-on:click.self="doThat">...</div>
<!-- 点击事件将只会触发一次 -->
<a v-on:click.once="doThis"></a>

<!-- 滚动事件的默认行为 (即滚动行为) 将会立即触发 -->
<!-- 而不会等待 `onScroll` 完成  -->
<!-- 这其中包含 `event.preventDefault()` 的情况 -->
<div v-on:scroll.passive="onScroll">...</div>
```  

### 按键修饰符
### 系统修饰键
### 标按钮修饰符
### 缩写
v-bind
```
<!-- 完整语法 -->
<a v-bind:href="url">...</a>
<!-- 缩写 -->
<a :href="url">...</a>
```
```
<!-- 完整语法 -->
<a v-on:click="doSomething">...</a>
<!-- 缩写 -->
<a @click="doSomething">...</a>
```

## 二、生命周期钩子函数
生命周期钩子的this上下文指向调用它的Vue实例:

created  Vue实例被创建之后

mounted 实例被挂载之后

## 三、模板语法
vue.js将DOM绑定至底层Vue实例的数据。

在底层实现上，Vue将模板编译成虚拟DOM渲染函数。

Vue也可允许你不用模板，直接写渲染（render）函数。

模板里面可以使用javaScript表达式（每个绑定都只能包含单个表达式）

```
{{ number + 1 }}

{{ ok ? 'YES' : 'NO' }}

{{ message.split('').reverse().join('') }}

<div v-bind:id="'list-' + id"></div>
```
>不要在选项属性或回调上使用[箭头函数](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Functions/Arrow_functions)，比如 `created: () => console.log(this.a)` 或 `vm.$watch('a', newValue => this.myMethod())`。因为箭头函数并没有 `this`，`this` 会作为变量一直向上级词法作用域查找，直至找到为止，经常导致 `Uncaught TypeError: Cannot read property of undefined` 或 `Uncaught TypeError: this.myMethod is not a function` 之类的错误。

## 四.计算属性、监听、过滤
###### 计算属性
 计算属性的出现是为了解决模板逻辑过重的问题。 计算属性的出现是为了解决模板逻辑过重的问题。
 ```
<div id="example">
  <p>Original message: "{{ message }}"</p>
  <p>Computed reversed message: "{{ reversedMessage }}"</p>
</div>
 ```
 ```
 var vm = new Vue({
el: '#example',
data: {
message: 'Hello'
},
computed: {
// 计算属性的 getter
reversedMessage: function () {
// `this` 指向 vm 实例
return this.message.split('').reverse().join('')
}
}
})
 ```
 >`vm.reversedMessage` 的值始终取决于 `vm.message` 的值。


 ###### 计算属性缓存 vs 方法
 当然以上效果我们可以通过方法实现：
 ```
 <p>Reversed message: "{{ reversedMessage() }}"</p>
 ```
 ```
 // 在组件中
methods: {
reversedMessage: function () {
return this.message.split('').reverse().join('')
}
}
 ```
 >不同的是**计算属性是基于它们的响应式依赖进行缓存的**。只在相关响应式依赖发生改变时它们才会重新求值。这就意味着只要 `message` 还没有发生改变，多次访问 `reversedMessage` 计算属性会立即返回之前的计算结果，而不必再次执行函数。


 ###### 计算属性 vs 侦听属性
 当有一些数据需要随着其他数据变动而变动时，很容易滥用watch,通常更好的做法是使用计算属性而不是命令式的watch回调。
```
<div id="demo">{{ fullName }}</div>
```
```
var vm = new Vue({
  el: '#demo',
  data: {
    firstName: 'Foo',
    lastName: 'Bar',
    fullName: 'Foo Bar'
  },
  watch: {
    firstName: function (val) {
      this.fullName = val + ' ' + this.lastName
    },
    lastName: function (val) {
      this.fullName = this.firstName + ' ' + val
    }
  }
})
```
上面代码是命令式且重复的。将它与计算属性的版本进行比较：
```
var vm = new Vue({
  el: '#demo',
  data: {
    firstName: 'Foo',
    lastName: 'Bar'
  },
  computed: {
    fullName: function () {
      return this.firstName + ' ' + this.lastName
    }
  }
})
```
## 五.key
用key管理可复用的元素

Vue 会尽可能高效地渲染元素，通常会复用已有元素而不是从头开始渲染。

例如，如果你允许用户在不同的登录方式之间切换：
```
<template v-if="loginType === 'username'">
  <label>Username</label>
  <input placeholder="Enter your username">
</template>
<template v-else>
  <label>Email</label>
  <input placeholder="Enter your email address">
</template>
```
那么在上面的代码中切换 `loginType` 将不会清除用户已经输入的内容。因为两个模板使用了相同的元素，`<input>` 不会被替换掉——仅仅是替换了它的 `placeholder`。

这样也不总是符合实际需求，所以 Vue 为你提供了一种方式来表达“这两个元素是完全独立的，不要复用它们”。只需添加一个具有唯一值的 `key` 属性即可：

```
<template v-if="loginType === 'username'">
  <label>Username</label>
  <input placeholder="Enter your username" key="username-input">
</template>
<template v-else>
  <label>Email</label>
  <input placeholder="Enter your email address" key="email-input">
</template>
```
## 六、组件和插槽
### 父子组件传值
### 插槽  (内容分发)
>在 2.6.0 中，我们为具名插槽和作用域插槽引入了一个新的统一的语法 (即 `v-slot` 指令)。它取代了 `slot` 和 `slot-scope` 这两个目前已被废弃但未被移除且仍在[文档中](https://cn.vuejs.org/v2/guide/components-slots.html#%E5%BA%9F%E5%BC%83%E4%BA%86%E7%9A%84%E8%AF%AD%E6%B3%95)的特性。新语法的由来可查阅这份 [RFC](https://github.com/vuejs/rfcs/blob/master/active-rfcs/0001-new-slot-syntax.md)。

插槽跟组件有什么区别？