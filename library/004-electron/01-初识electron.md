## 关于electron

Electron是由Github开发，用HTML,CSS和JavaScript来构建跨平台桌面应用程序的一个开源库。

Electron通过将Chromium和Node.js合并到同一个运行环境中，并将其打包为Mac,Windows和Linux系统下的应用来实现这一目的。

Electron于2013年作为构建Github上可编辑的文本编辑器Atom的框架而被开发出来。这两个项目在2014春季开源，目前它已成为开源开发者、初创企业和老牌公司常用的开发工具。


## 延伸概念
主进程：连接操作系统和渲染进程，是页面和计算机沟通的桥梁

渲染进程:就是前端环境，只是载体变了，从浏览器变成了window