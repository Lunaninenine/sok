Promise 是抽象的异步处理对象,可以解决回调地狱的问题.

>Promise相对其他异步处理方式最大的好处是在异步执行的流程中，把执行代码和处理结果的代码清晰地分离了.

### 状态

* Pending：进行中
* Resolved(Fulfilled)：已完成
* Rejected：已失败
  
### Promise状态的改变只有两种：

1.Pending --> Resolved

2.Pending --> Rejected

>这意味着，一个Promise对象resolve之后，状态就一直停留在Resolved那里了，反过来reject也一样。
 这种特点的结果是，Promise对象的状态改变之后，你再给它添加回调函数，这个函数也会执行。
 这跟事件监听器很不一样 —— 你一旦错过某个事件，就没办法再捕获他了，除非这个事件再次发生。

 ## .then()和.catch()

* resolve对应promise对象的.then()方法
* reject对应promise对象的.catch()方法
  
实例：

```js
var p = new Promise(function(resolve,reject){
 //异步操作
 //...
 //举例：
var a = 1;
if (a == 1){
    resolve(a);
   } else {
    reject(error);
   }
  })
  
 p.then(function (params) { 
    console.log(params);
   }).catch(function (status) {
    console.log('error');
  });
```
输出结果：
`1`

### promise可链式调用then和catch

>不管是then方法还是catch方法返回的都是一个新的Promise实例，这意味着Promise可以链式调用then和catch，每一个方法的返回值作为下一个方法的参数

*注意用时一个promise对象对应一个then和catch，.then时不要丢catch。*

实例：

```js
​        var p = new Promise(function(resolve,reject){

​           resolve()

​        })

​        p.then(function (params) { //抛出错误

​            console.log('我是第一个then方法')

​            console.log(1 / x)

​        }).then(function(){//此方法不会执行

​            console.log('我是第二个then方法');

​        }).catch(function (status) {//接受错误并打印出错误

​            console.log('error');

​        }).then(function(){

​            console.log('我是第三个then方法');

​        });
```

输出结果：
```js
我是第一个then方法
error
我是第三个then方法
```
## Promise.resolve() 和 Promise.reject()

>两者都是new promise()的快捷方式。

Promise.resolve(value)是下面代码的语法糖：

```js
 new Promise(function (resolve) {
      resolve(value)
    })
```
所以：

```js
Promise.resolve(333).then(function(value){

​           console.log(value);

​       }) 

​       //输出结果：333

​    // 等同于

​    var promise = new Promise(function(resolve){

​        resolve(333)

​    })

​    promise.then(function(value){

​        console.log(value);

​    });
```

Promise.reject(value)是下面代码的语法糖：

```js
 new Promise(function(resolve,reject){
        reject(new Error("出错了"));
    });
```

## Promise.all() 和 Promise.race()

并行执行异步任务使用Promise.all，
Promise.all()接收一个Promise对象的数组作为参数，当这个数组里的所有Promise对象全部变为resolve的时候，该方法才resolve.

```js
function timerPromisefy(delay) {
    return new Promise(function (resolve) {
        setTimeout(function () {
            resolve(delay);
        }, delay);
    });
}
// 当数组中所有Promise对象被resolve之后，该方法才返回
Promise.all([
    timerPromisefy(1),
    timerPromisefy(32),
    timerPromisefy(64),
    timerPromisefy(128)
]).then(function (value) {
    console.log(value);    
});

//输出： [ 1, 32, 64, 128 ]
```
Promise.race()使用方法和Promise.all一样，接收一个Promise对象数组为参数
只要其中一个Promise对象变为Resolved或者Rejected状态，该方法返回，进行后面的处理。

```js
function timerPromisefy(delay) {
    return new Promise(function (resolve) {
        setTimeout(function () {
            resolve(delay);
        }, delay);
    });
}
// 任何一个promise变为resolve或reject 的话程序就停止运行
Promise.race([
    timerPromisefy(1),
    timerPromisefy(32),
    timerPromisefy(64),
    timerPromisefy(128)
]).then(function (value) {
    console.log(value);    
});

// 输出： 1
```

## 使用Promise封装AJAX

```js
        function get(url) {
            // 返回一个Promise对象
            return new Promise(function (resolve, reject) {
                // 创建一个XHR对象
                var req = new XMLHttpRequest() || new ActiveXObject('Microsoft.XMLHTTP')
                req.open('GET', url, true)
                // 使用req.onload监听req的状态
                req.onload = function () {
                    if (req.readyState == 4 && req.status == 200) {
                        resolve(req.response)
                    } else {
                        reject(Error(req.statusText))
                    }
                }

                // 网络错误
                req.onerror = function () {
                    reject(Error("Network Error"));
                };

                // 发送请求
                req.send();
            });
        }
        get("http://something").then(function (response) {
            console.log(response);
        }).catch(function (error) {
            console.log(error);
        })
```

*有一个地方需要注意,在`get()`函数里面调用了`req.onload`而不是req.onreadystatechange，因为Promise的状态的改变是单向的，一次性的，一旦改变，状态就凝固了，而我们在`if...else`语句中改变了Promise的状态，如果使用`req.onreadystatechange`只会被调用一次，永远只能得到error，因为req的state一旦改变，`req.onreadystatechange`就会被调用，如果时这样，我们永远只能捕捉到req的state为2的时候！*

## 兼容性
IE内核是不支持Promise的.
如果要用 Promise 的话，引用 **bluebird.js**,bluebird.js封装了 ES6 原生 Promise.

*也可以使用babel把工程里所有的ES6转成ES5*

### Vue/react 项目中的 Promise 兼容问题
1.安装 babel-polyfill （babel-polyfill可以模拟ES6使用的环境，可以使用ES6的所有新方法）
`npm install --save babel-polyfill`

2.在 Webpack/Node中使用

在webpack.config.js文件中，使用

```js
module.exports = {
entry: {

        app: ["babel-polyfill", "./src/main.js"]//加上babel-polyfill引用
    }
};
```